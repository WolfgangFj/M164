# Tag 7

## Inhalt

- [Notizen zu Subqueries (Unterabfragen)](#notizen-zu-subqueries-unterabfragen)
- [Datensicherung und Backup bei Datenbanken](#datensicherung-und-backup-bei-datenbanken)
  - [Bedeutung der Datensicherung](#bedeutung-der-datensicherung)
  - [Arten von Backups](#arten-von-backups)
  - [Backup-Methoden](#backup-methoden)
  - [Speicherung und Sicherheit](#speicherung-und-sicherheit)
  - [Tools für die Durchführung von Backups](#tools-für-die-durchführung-von-backups)
  - [Logische Backups](#logische-backups)
  - [Physische Backups](#physische-backups)
- [Logisches Backup via MySQL](#logisches-backup-via-mysql)
  - [Aufgabe 1: Logisches Backup](#aufgabe-1-logisches-backup)
  - [Aufgabe 2: Backup-File analysieren und verifizieren](#aufgabe-2-backup-file-analysieren-und-verifizieren)
  - [Aufgabe 3: Backup-Strategien](#aufgabe-3-backup-strategien)

## Notizen zu Subqueries (Unterabfragen)

## Datensicherung und Backup bei Datenbanken

### Bedeutung der Datensicherung

Datenbanksicherung ist sehr wichtig, da wenn die DB nicht läuft, in der Regel nichts mehr im Betrieb funktioniert. Z.B. bei uns im Betrieb fiel einmal der MySQL-Server aus, wodurch alle VD-Applications nicht erreichbar waren, die Personalverwaltung gestoppt wurde und interne Telefonlisten nicht verfügbar waren.

### Arten von Backups

- **Online-Backups:** Sicherungen ohne Unterbrechung des Datenbankbetriebs.
- **Offline-Backups:** Sicherungen während die DB offline ist, was ein Herunterfahren erfordert.

### Backup-Methoden

- **Voll-Backup:** Sichert alle Daten jedes Mal, benötigt viel Speicherplatz.
- **Differentielles Backup:** Sichert nur geänderte oder neue Dateien seit dem letzten Voll-Backup.
- **Inkrementelles Backup:** Sichert nur geänderte oder neue Dateien seit dem letzten Backup.

### Speicherung und Sicherheit

- Backups sollen auf externen Medien gespeichert werden, die vor Diebstahl und Brandschäden sicher sind.
- Verschlüsselung der gesicherten Daten ist wichtig, da Datenbanken sehr oft wichtige persönliche Daten enthalten.

### Tools für die Durchführung von Backups

- MySQLDump: Integriertes Tool für die Sicherung.
- phpMyAdmin: Web-GUI-Tool für SQL-Datenbanken.
- BigDump: Ergänzung zu phpMyAdmin für grosse Backup-Dateien.
- HeidiSQL: Backup-Lösung für Windows ohne Probleme mit grossen Backups.
- Mariabackup: Open-Source-Tool für physische Online-Backups von InnoDB-, Aria- und MyISAM-Tabellen.

### Logische Backups

- SQL-Anweisungen wie CREATE DATABASE, CREATE TABLE und INSERT.
- Flexibler, da sie auf anderen PCs, MariaDB-Versionen oder DBMS wiederhergestellt werden können.
- Grösser als physische Sicherungen.
- Benötigen mehr Zeit für Sicherung und Wiederherstellung.

### Physische Backups

- Kopien der einzelnen Datendateien oder Verzeichnisse.
- Weniger flexibel, nicht geeignet für deutlich andere Hardware, DBMS oder MariaDB-Versionen.
- Kleinere Speichergrösse im Vergleich zu logischen Backups.
- Schnellere Sicherung und Wiederherstellung im Vergleich zu logischen Backups.

## Logisches Backup via MySQL

### Aufgabe 1: Logisches Backup

```sh
C:\XAMPP\MYSQL\BIN\mysqldump -u root -p --port=3306 tourenplaner > ~/Tourenplaner_Backup.sql

```
# Aufgabe 2: Backup-File analysieren und verifizieren

1. **Inhalt:** Das Dumpfile enthält SQL-Statements für die Wiederherstellung und auch die Daten, die in der DB gespeichert waren.

2. **Überprüfen des Dump-Files (Restore):**
   - **Datenbank löschen:**
     ```sql
     DROP DATABASE tourenplaner;
     ```
   - **Datenbank neu erstellen:**
     ```sql
     CREATE DATABASE tourenplaner;
     ```
   - **Dump-File ausführen:**
     ```sql
     USE tourenplaner;
     SOURCE C:\MySQLBackup\dump.sql;
     ```

## Aufgabe 3: Backup-Strategien

- **Erstellter Backup in Aufgabe 1:** Ein logisches Backup, erstellt mit mysqldump.

- **Nachteile des logischen Backups:**
  - Ist langsamer als physische Backups, besonders bei grossen Datenbanken.
  - Wiederherstellung ist ebenfalls langsamer.
  - Grösser, da Daten im Textformat vorliegen.

- **Unterschied zwischen Online- und Offline-Backups:**
  - **Online-Backup:** Läuft währenddem die DB aktiv ist.
  - **Offline-Backup:** Die DB muss dafür offline sein.

- **Snapshot Backup:** Ein Snapshot-Backup ist ähnlich wie bei VMs; es erstellt ein Abbild des aktuellen Zustands der DB, was eine schnelle Wiederherstellung ermöglicht.


