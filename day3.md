# Tag 3

# 2. Inhalt:
- [2. Inhalt:](#2-inhalt)
- [3. Repetition Identifying Beziehung](#3-repetition-identifying-beziehung)
- [4. Repetition Datentypen](#4-repetition-datentypen)
- [5. Insert Aufgaben:](#5-insert-aufgaben)


# 3. Repetition Identifying Beziehung

> `PRIMARY KEY ('PK_Ausweis_ID', 'FK_Person')`
> Das Kleidungsstück kann auch ohne Perso existieren.
> Der Primary Key ist hier das Kleidungsstück und die Person.

# 4. Repetition Datentypen

| Datentyp         | Beschreibung                                                                          | Beispiel(e)                         |
|------------------|---------------------------------------------------------------------------------------|-------------------------------------|
| Integer (int)    | Ganze Zahlen, positiv oder negativ, ohne Dezimalstellen                               | 42, -7, 0                           |
| Float (float)    | Gleitkommazahlen, die Dezimalstellen haben können                                     | 3.14, -0.001, 2.0                   |
| Double (double)  | Gleitkommazahlen mit doppelter Genauigkeit                                            | 3.1415926535, 2.7182818284          |
| Boolean (bool)   | Wahrheitswerte, die nur zwei Werte annehmen können: wahr (true) oder falsch (false)   | true, false                         |
| Character (char) | Ein einzelnes Zeichen                                                                 | 'a', 'Z', '1', '!'                  |
| String (string)  | Eine Folge von Zeichen                                                                | "Hallo", "1234", "Guten Tag!"       |
| Array (array)    | Eine Sammlung von Elementen des gleichen Typs                                         | [1, 2, 3], ['a', 'b', 'c']          |
| List (list)      | Eine geordnete Sammlung von Elementen, die auch unterschiedlichen Typs sein können    | [1, "zwei", 3.0, True]              |
| Dictionary (dict)| Eine Sammlung von Schlüssel-Wert-Paaren                                               | {"Name": "Max", "Alter": 28}        |

## 5. Insert Aufgaben:

**a. Legen Sie mit der Kurzform einen Kunden mit folgenden Daten an: Heinrich Schmitt aus Zürich, Schweiz (Schweiz hat die land_id 2).**

-INSERT INTO kunden (vorname, nachname, land_id, wohnort) VALUES ('Heinrich', 'Schmitt', 2, 'Zürich');

**b. Legen Sie mit der Kurzform eine Kundin mit folgenden Daten an: Sabine Müller aus Bern, Schweiz (Schweiz hat die land_id 2).**

-INSERT INTO kunden (vorname, nachname, land_id, wohnort) VALUES ('Sabine', 'Müller', 2, 'Bern');

**c. Legen Sie mit der Kurzform einen Kunden mit folgenden Daten an: Markus Mustermann aus Wien, Österreich (Österreich hat die land_id 1).**

-INSERT INTO kunden (vorname, nachname, land_id, wohnort) VALUES ('Markus', 'Mustermann', 1, 'Wien');

**d. Legen Sie mit der Langform einen Kunden mit folgenden Daten an: Herr Maier.**

-INSERT INTO kunden (vorname, nachname, land_id, wohnort) VALUES ('NULL', 'Maier', NULL, 'NULL');

**e. Legen Sie mit der Langform einen Kunden mit folgenden Daten an: Herr Bulgur aus Sirnach.**

-INSERT INTO kunden (vorname, nachname, land_id, wohnort) VALUES ('NULL', 'Bulgur', NULL, 'Sirnach');

**f. Legen Sie mit der Langform einen Kunden mit folgenden Daten an: Maria Manta.**

-INSERT INTO kunden (vorname, nachname, land_id, wohnort) VALUES ('Maria', 'Manta', NULL, 'NULL');


**a. INSERT INTO (nachname, wohnort, land_id) VALUES ('Fesenkampp', 'Duis-burg', 3);**

Der Tabellenname fehlt (kunden).

**b. INSERT INTO kunden ('vorname') VALUES ('Herbert');**

Vorname ist kein String, dort sollte man ohne Apostroph schreiben.

**c. INSERT INTO kunden (nachname, vorname, wohnort, land_id) VALUES ('Schulter', 'Albert', 'Duisburg', 'Deutschland');**

Die Land_id hat eine ID-Nummer und ist kein String.

**d. INSERT INTO kunden ('', 'Brunhild', 'Sulcher', 1, 'Süderstade');**

Es fehlen die Attribute und man kann den String nicht leer lassen, dort soll entweder NULL oder NOT NULL stehen..

**e. INSERT INTO kunden VALUES ('Jochen', 'Schmied', 2, 'Solingen');**

Attribute fehlen.

**f. INSERT INTO kunden VALUES ('', 'Doppelbrecher', 2, '');**

Die String sind leer und die Attribute fehlen.

**g. INSERT INTO kunden (nachname, wohnort, land_id) VALUES ('Christoph', 'Fesenkampp', 'Duisburg', 3);**

Bei den Attributen fehlt Vorname.

**h. INSERT INTO kunden (vorname) VALUES ('Herbert');**

Ist korrekt.

**i. INSERT INTO kunden (nachname, vorname, wohnort, land_id) VALUES (Schulter, Albert, Duisburg, 1);**

Vorname, Nachname und Wohnort sollte in Strings sein.

**j. INSERT INTO kunden VALUE ('', "Brunhild", "Sulcher", 1, "Süderstade");**

Es gibt 2 verschiedene Apostrophen einmal ' und einmal ".

**k. INSERT INTO kunden VALUE ('', 'Jochen', 'Schmied', 2, Solingen);**

Solingen ist nicht in Strings.

## Update, Alter, Delete und Drop

**Beim Regisseur «Cohen» fehlt der Vorname. Vervollständigen sie den Regisseur Namen mit dem Vornamen «Etan».**

UPDATE dvd_sammlung SET regisseur = 'Etan Cohen' WHERE regisseur = 'Cohen';

**Der Film «Angst» dauert nicht 92 Minuten, sondern 120 Minuten. Korrigieren Sie.**

UPDATE dvd_sammlung SET laenge_minuten = '120' WHERE film = 'Angst';

**DVD gibt es nicht mehr. Das Sortiment wurde durch «Bluray» Medien ersetzt. Nennen Sie die Tabelle um nach «bluray_sammlung».**

RENAME TABLE dvd_sammlung TO bluray_sammlung

**Eine neue Spalte «Preis» soll hinzugefügt werden.**

ALTER TABLE bluray_sammlung ADD COLUMN preis DECIMAL(10,2);

**Der Film «Angriff auf Rom» von Steven Burghofer wurde aus dem Sortiment entfernt. Bereinigen Sie die Tabelle.**

DELETE FROM dvd_sammlung WHERE film = 'Angriff auf Rom' AND regisseur = 'Steven Burghofer';


**Die Spalte «filme» soll nach «kinofilme» umbenannt werden.**

ALTER TABLE bluray_sammlung WHERE film 

**Die Spalte Nummer wird nicht mehr benötigt. Löschen Sie sie.**

ALTER TABLE dvd_sammlung CHANGE COLUMN filme kinofilme VARCHAR(255);


**Der Filmverleih rentiert nicht mehr. Die Firma wurde geschlossen und folglich werden alle Daten eliminiert. Löschen Sie die Tabelle.**

DROP TABLE dvd_sammlung;