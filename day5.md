# Tag 5

# 2. Inhalt:
- [2. Inhalt:](#2-inhalt)
  - [2.1 Notizen Lehrerinput und Theorie](#21-notizen-lehrerinput-und-theorie)
    - [2.1.1 Notizen Löschen in DBs](#211-notizen-löschen-in-dbs)
    - [2.1.2 Notiz über die FK-Constraint-Options](#212-notiz-über-die-fk-constraint-options)
  - [2.2 Fragen](#22-fragen)
    - [2.2.1 Referentielle Datenintegrität](#221-referentielle-datenintegrität)
    - [2.2.2 Referentielle Datenintegrität Fortgeschritten](#222-referentielle-datenintegrität-fortgeschritten)

### 2.1.1 Notizen Löschen in Datenbanken

Das Löschen von Datensätzen in Datenbanken sollte mit Bedacht erfolgen, da es zu einem unerwünschten Informationsverlust führen kann. In professionellen Anwendungen wird der direkte Einsatz des SQL-Befehls `DELETE` oft vermieden. Stattdessen werden Datensätze als inaktiv markiert oder zusätzliche Informationen wie Austrittsdaten hinzugefügt, um die Datenhistorie zu bewahren. Beispielsweise sollten Mitarbeiterdatensätze nicht gelöscht werden, um frühere Aktivitäten nachvollziehen zu können. Ähnliches gilt für Kassensysteme, wo statt Löschungen Stornierungen genutzt werden, um die Integrität der Daten zu bewahren und Missbrauch zu verhindern.


- **Eindeutigkeit/Datenkonsistenz**: Jeder Datensatz sollte einzigartig sein, um doppelte Einträge zu vermeiden.
- **Referenzielle Integrität**: Beziehungen zwischen Tabellen müssen konsistent bleiben.
- **Datentypen**: Richtiger Datentyp wählen  -> z.B. Telefonnummer als String statt als Integer.
- **Datenbeschränkungen**: Regeln stellen sicher, dass die Daten gültig sind -> Email Feld muss "@" enthalten...
- **Validierung**: Daten werden vor dem Einfügen überprüft.


### 2.1.2 Notiz über die FK-Constraint-Options
Fremdschlüssel-Constraints -> referenzielle Integrität zwischen Tabellen in DB aufrechterhalten. <br>
Optionen beim Löschen eines Datensatzes:

- **ON DELETE NO ACTION**: -> Beim Löschen nichts machen.
- **ON DELETE CASCADE**: -> Datensätzen in allen FK's ach löschen.
- **ON DELETE SET NULL**: -> Beim Löschen von Daten in der Primärtabelle werden die FK's auf NULL gesetzt.
- **ON DELETE DEFAULT**: -> Beim Löschen von Daten in der Primärtabelle werden die FK's auf einen vorher definierten Wert gesetzt.


## 2.2 Fragen


### 2.2.1 Referentielle Datenintegrität

#### 2.2.2 Aufgabe 1

Weshalb können in professionellen Datenbanken nicht einfach so Daten gelöscht werden?

1. Das Löschen des Datensatzes mit 4000 Basel geht nicht, weil die ref. Integrität über den FK-Constraint überwacht wird. Zudem ist die Beziehung eine "Identifying Relationship":
--
-- Table structure for table `tbl_stationen`
--

CREATE TABLE `tbl_stationen` (
  `FS_ID_Fahrt` int NOT NULL,
  `FS_ID_Ort` int NOT NULL,
   ...
  PRIMARY KEY (`FS_ID_Fahrt`,`FS_ID_Ort`),  -- <<-- Identifying Relationship
  ...
  CONSTRAINT `fk_{ACFE9F9D-C013-4C23-9E93-8A7A284EF6AF}` 
    FOREIGN KEY (`FS_ID_Ort`) REFERENCES `tbl_orte` (`ID_Ort`),  -- <<-- Überwachung
  ...
);__

Wer stellt die referentielle Integrität sicher?
> in SQL kann man den Datentyp angeben, z.B. VARCHAR(35), dies definierte einen String von 35 Character länge.

### Aufgabe 2

Als Datenbank-Entwickler stellen Sie fest, dass bei der Dateneingabe ein Fehler passiert ist. Anstatt «4000 Basel» sollte «3000 Bern» heissen. Versuchen Sie in der Tabelle «tbl_ort» die Ortschaft «Basel» zu löschen.
Was stellen sie fest? Erklären Sie!

2.	Da die tbl_orte.PLZ keine ID ist, kann der tbl_Orte-Datensatz einfach mit Update SET geändert werden.
Tabelle Ort:

```SQL
--
-- Table structure for table `tbl_orte`
--

DROP TABLE IF EXISTS `tbl_orte`;
CREATE TABLE `tbl_orte` (
  `ID_Ort` int NOT NULL AUTO_INCREMENT,  -- <<-- ID als Referenz bleibt erhalten
  `PLZ` varchar(6) DEFAULT NULL,  -- <<-- PLZ ist keine ID!
  `Ortsbezeichnung` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID_Ort`)
) AUTO_INCREMENT=6;

```

Allenfalls sollte vorher geschaut werden, ob es 3000 Bern schon gibt! Dann müssten alle tbl_Stationen-Datensätze, die auf BASEL zeigen, neu auf Bern "umgebogen" werden bevor Basel-Datensatz geläscht wird.

### 2.2.2 Referentielle Datenintegrität Fortgeschritten

1. Löschen Sie alle Orte: (CASCADE)
DELETE FROM orte;

2. Wenden Sie im SQL-Script RESTRICT auf ON UPDATE und ON DELETE an und laden Sie die DB wieder neu 

Nach dem Setzten von on Restrict funktioniert das löschen nicht mehr.


3. Löschen Sie via SQL-Befehl den Ort Emmendingen:
```sql
DELETE FROM orte
WHERE name = 'Emmendingen';
```
Geht nicht aufgrund eines Foreign Key Constraint Fehlers.

4. Ändern Sie die Postleitzahl von Musterhausen:
```sql
UPDATE orte 
SET postleitzahl = '99999' 
WHERE name = 'Musterhausen';
```
Geht nicht aufgrund eines Foreign Key Constraint Fehlers.



5. Sie haben festgestellt, dass sich Änderungen nicht durchführen lassen. Ändern Sie die CONSTRAINTS auf SET NULL und NO ACTION und probieren Sie sie bspw. mit Nr. 3 und 4 nochmals aus.


```sql
UPDATE kunden
SET postleitzahl_FK = NULL
WHERE postleitzahl_FK = (SELECT postleitzahl FROM orte WHERE name = 'Emmendingen');

DELETE FROM orte
WHERE name = 'Emmendingen';
```

```sql
UPDATE kunden
SET postleitzahl_FK = NULL
WHERE postleitzahl_FK = (SELECT postleitzahl FROM orte WHERE name = 'Musterhausen');

UPDATE orte 
SET postleitzahl = '99999' 
WHERE name = 'Musterhausen';
```
Nach dem setzten von `SET NULL` und `NO ACTION` funktionieren die Befehle.