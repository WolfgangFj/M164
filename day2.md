# Tag 2

# 2. Inhalt:
- [2. Inhalt:](#2-inhalt)
- [3. Generalisieren vom Tourenplan](#3-generalisieren-vom-tourenplan)
- [4. Generalisieren Beispiele](#4-generalisieren-beispiele)
- [5. Identifying and Non-Identifying Relation](#5-identifying-and-non-identifying-relation)
- [6. Was ist ein DBMS](#6-was-ist-ein-dbms)

# 3. Generalisieren vom Tourenplan
Beim Tourenplan sollte einen Mitarbeiter Tabelle erstellt werden, da sonst, falls ein Mitarbeiter Disponent und Fahrer ist, wir eine Redundanz haben.

![Tourenplan](./Bilder/touren.png)

# 4. Generalisieren Beispiele

Wir haben uns das Beispiel der verschiedenen Flugklassen(Business, Economy, First, etc.) überlegt.

Weitere Beispiele: 
- Fahrer, Tour, Funktion
- PKW, LKW, KFZ
- Äpfel, Birnen, Obst

# 5. Identifying and Non-Identifying Relation

Gestrichelte bei losen, sich ändernde Beziehungen(Non-Identifying), durchgezogene Linien bei fixed Verbindungen(Indetifying), die nicht mehr geändert werden.

Beispiele:
- Gattung, Art
- Auto, Räder

# 6. Was ist ein DBMS

**Merkmale:**
1. **Integrierte Datenhaltung**: Einheitliche Verwaltung aller Daten, komplexe Beziehungen und schnelle Verknüpfung zusammenhängender Daten.
2. **Sprache**: Bereitstellung von Datenbanksprachen (Query Language) für Datenanfrage (retrieval), Datenmanipulation (DML), Datenbankverwaltung (DDL) und Berechtigungssteuerung (DCL). Benutzeroberflächen für verschiedene Nutzergruppen, einschliesslich Webzugriff.
3. **Katalog**: Zugriff auf Metadaten der Datenbank.
4. **Benutzersichten**: Verschiedene Sichten (views) für unterschiedliche Benutzergruppen, definiert im externen Schema.
5. **Konsistenzkontrolle**: Sicherstellung der Datenintegrität durch Integritätsbedingungen und Gewährleistung der physischen Integrität.
6. **Datenzugriffskontrolle**: Verhinderung unautorisierter Zugriffe durch Regeldefinitionen.
7. **Transaktionen**: Zusammenfassung mehrerer Änderungen zu Transaktionen mit Atomarität und Dauerhaftigkeit.
8. **Mehrbenutzerfähigkeit**: Synchronisation konkurrierender Transaktionen, um Beeinflussungen zu vermeiden.
9. **Datensicherung**: Wiederherstellung eines korrekten Datenbankzustands bei Fehlern.

**Vorteile:**
1. **Nutzung von Standards**: Erleichterung der Einführung und Umsetzung zentraler Standards.
2. **Effizienter Datenzugriff**: Techniken für effizientes Speichern und Wiederauslesen grosser Datenmengen.
3. **Kürzere Softwareentwicklungszeiten**: Schnellere Anwendungsentwicklung durch bereitgestellte Funktionen und Datenbanksprachen.
4. **Hohe Flexibilität**: Modifikation der Datenbankstruktur ohne grosse Konsequenzen für bestehende Daten und Anwendungen.
5. **Hohe Verfügbarkeit**: Datenbank steht allen Benutzern gleichzeitig zur Verfügung, Änderungen werden sofort sichtbar.
6. **Grosse Wirtschaftlichkeit**: Zentralisierung reduziert Betriebs- und Verwaltungskosten durch Investition in leistungsstärkere Hardware.

**Nachteile:**
1. **Hohe Anfangsinvestitionen**: Kosten für Hardware und Datenbanksoftware.
2. **Weniger Effizienz für spezialisierte Anwendungen**: Allzweck-Software ist nicht optimal für alle Anforderungen.
3. **Mehrkosten für Datensicherheit und Synchronisation**: Zusätzliche Kosten für die Bereitstellung dieser Funktionen.
4. **Erforderliches hochqualifiziertes Personal**: Bedarf an Datenbankdesignern und -administratoren.
5. **Verwundbarkeit durch Zentralisierung**: Zentralisierung kann ein Risiko darstellen.




