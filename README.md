# Repository zu Modul 164
<br />

## Thema: Datenbanken erstellen und Daten einfügen

# Inhalt:

[Tag 1 Anschauen m162](./day1.md)  <br>
[Tag 2 Einführung DBMS](./day2.md)  <br>
[Tag 3 DBMS Allgemein](./day3.md)  <br>
[Tag 4 Anschauen SQL](./day4.md)  <br>
[Tag 5 SQL mit Join](./day5.md)  <br>
[Tag 6 Anschauen von Subqueries (Krank)](./day6.md)  <br>
[Tag 7 verschiedene Backup-lösungen](./day7.md)  <br>
[Tag 8 Freifächer](./day8.md)  <br>
[Tag 9 LB2 Test](./day9.md)