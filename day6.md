# Tag 6

## Krank gewesen habe trotzdem versucht ein paar Aufgaben zu machen.

# 2. Inhalt:
- [2. Inhalt:](#2-inhalt)
- [3. Einstellungen für CSV import](#4-einstellungen-für-csv-import)
- [4. Import](#5-import)


# 3. Einstellungen für CSV import
Auf dem Server war es sehr einfach, lediglich die folgenden Commands

'SET GLOBAL local infile=l;'
'SHOW GLOBAL VARIABLES LIKE 'local_infile';'
SHOW VARIABLES LIKE 'secure_file_priv' ;

In meinem DB-Client, die VScode Extensions war es etwas schwieriger: <br>
SET sql_safe_updates = 1
SET GLOBAL local_infile = 1;


# 4. Import
Welcher Zeichensatz ist standardmässig bei ihrem DBMS eingestellt?
> SELECT @@character_set_database, @@collation_database; <br>
> +--------------------------+----------------------+ <br>
> | @@character_set_database | @@collation_database | <br>
> +--------------------------+----------------------+ <br>
> | latin1                   | latin1_swedish_ci    | <br>
> +--------------------------+----------------------+ <br>


Versuchen Sie, das CSV File Personen in eine Tabelle einzulesen. Untersuchen Sie die CSV-Datei zuerst: Trennzeichen(Delimiter), ", Spaltennamen, Zeichensatz, Datumformat, ...
> Ich habe die Datei untersucht, und die Datei ist mit dem Trennzeichen ";" und Line Terminator "\n"
> ```sql
> CREATE TABLE personen (
>    id INT AUTO_INCREMENT PRIMARY KEY,
>    Vorname VARCHAR(255),
>    Nachname VARCHAR(255),
>    Geburtsdatum DATE
> );
> ```


> ```sql
>    LOAD DATA INFILE 'C:/Users/wolfg/m164/m164/day6/personen.csv'
>    INTO TABLE personen
>    FIELDS TERMINATED BY ';'
>    LINES TERMINATED BY '\n'
>    IGNORE 1 ROWS
>    (id, Vorname, Nachname, Geburtsdatum);
> ```