# Tag 1

# 2. Inhalt:
- [2. Inhalt:](#2-inhalt) <br>
- [3. Recap m164](#3-recap-m164) <br>
- [4. Fragen:](#4-fragen) <br>
- [5. Tourenplaner](#5-tourenplaner) <br>
- [6. Installation Xampp](#6-installation-xampp) <br>

# 3. Recap m164

# 4. Fragen:

#### 4.0.0.1. Wissenstreppe? 
> Buchstaben -> Daten -> Informationen -> Wissen -> Kompetenz
![](./Bilder/wissen.png)

#### 4.2 Nennen Sie diese der Reihe nach und machen Sie ein Beispiel mit einem Wechselkurs.
> Zeichen: 1
> Daten: 1Fr.
> Information: Der Wechselkurs ist 0.99 EUR für 1 Fr.
> Wissen: Ein tiefer Wechselkurs von Eur to CHF sorgt für ein ungleichgewicht.
> Kompetenz: Ein erfahrener Trader nutzt sein Wissen um gezielt in die Währungen zu investieren um Gewinn zu generieren.

#### 4.3 Wie werden Netzwerk-Beziehungen im logischen Modell abgebildet?
> Durch beziehungen zwischen Entitäten:
> Eins-zu-eins (1:1):  A ist mit genau einem B verbunden.
> Eins-zu-viele (1:n): A ist mit mehreren B's verbunden.
> Viele-zu-viele (n:m): Mehrere A's sind mit mehreren B's verbunden.

#### 4.4 Was sind Anomalien in einer Datenbasis? Welche Arten gibt es?
> Einfügeanomalie: Probleme beim Hinzufügen von Daten.
> Löschanomalie: Löschen von Daten, löscht Daten die noch benötigt werden.
> Änderungsanomalie: Änderungen an Daten die an mehreren Orten vorgenommen werden müssen.

#### 4.5 Gibt es redundante Daten?
> Ja, fast immer. Redundante Daten sind in vielen Datenbanken vorhanden, die können aber möglichst vermieden werden, um z.B. die Geschwindigkeit einer DB zu verbessern.

#### 4.6 Datenstrukturierung:  Welche zwei Aspekte können strukturiert werden?  
> Es gibt folgende Arten der Datenstrukturierung:
> Inhaltliche Strukturierung: Bedeutung und den Zusammenhang der Daten.
> Formale Strukturierung: Format und die Anordnung der Daten.

#### 4.7 Welche Kategorien (Abstufungen) gibt es bei der Strukturierung?  Und wie müssen die Daten in einer DB strukturiert sein?
> Unstrukturierte Daten: Daten ohne festgelegtes Format, z.B. Textdokumente, Videos.
> Mittelmässig-strukturierte Daten: Daten mit einer leichten Struktur, z.B. XML-Daten.
> Strukturierte Daten: Daten in einem festen Format, z.B. DBMS.

#### 4.8 Beschreiben das Bild mit den richtigen Fachbegriffen 
![](./Bilder/question6.png)
> 1: Tabellennamen
> 2: Datensatz
> 3: Attribut
> 4: Feld
> 5: Tabelle

#### 4.9 Welche (einschränkenden) Einstellungen zu den Attributen (z.B. ID) kennen Sie?  
> Primary Key (PK): Primärschlüssel.
> Not Null (NN): Der Wert kann nicht leer gelassen werden.
> Unique (UN): Der Wert darf nicht mehrmals vorkommen (einmalig).
> Foreign Key (FK): Ein Fremdschlüssel ist dazu da, um Daten aus einer anderen Tabelle zu verbinden.
> Auto Increment (AI): Ein „Auto Increment“-Feld wird automatisch mit einem eindeutigen Wert gefüllt, z.B. einer ID.