# Tag 4

# 2. Inhalt:
- [2. Inhalt:](#2-inhalt)
- [3. Aufgaben ALTER TABLE](#4-aufgaben-alter-table)
- [4. Mengenlehre](#5-mengenlehre)
- [5. Mengenlehre Aufgaben](#6-mengenlehre-aufgaben)


# 3. Aufgaben ALTER TABLE

1. **Fügen Sie ein paar Daten in die Tabellen tbl_Projekt,tbl_Passagier, tbl_Bus, tbl_Fahrer und tbl_Ausweis ein und überprüfen Sie die Beziehungen. Beachten Sie, dass Sie zuerst die Master-Tabelle füllen müssen, bevor Sie in der Detailtabelle FK-Werte setzen können!**
> ```sql
    INSERT INTO tbl_Fahrer (ID_Fahrer, Name) VALUES (1, 'Fahrer 1');
    INSERT INTO tbl_Projekt (ID_Projekt, Name) VALUES (1, 'Projekt 1');

    INSERT INTO tbl_Bus (ID_Bus, Bezeichnung, Kennzeichen, Anzahl_Plätze, FS_tbl_Fahrer) VALUES (1, 'Bus 1', 'KZ-123-AB', '30', 1);
    INSERT INTO tbl_Passagier (ID_Passagier, Name, Projekt_ID) VALUES (1, 'Passagier 1', 1);
    INSERT INTO tbl_Ausweis (ID_Ausweis, Nummer, Art, FS_tbl_Fahrer) VALUES (1, 'A1', 1, 1);

    SELECT * FROM tbl_Bus WHERE FS_tbl_Fahrer = 1;
    SELECT * FROM tbl_Passagier WHERE Projekt_ID = 1;
    SELECT * FROM tbl_Ausweis WHERE FS_tbl_Fahrer = 1;
    ```
>

1. **Was geschieht, wenn Sie z.B. bei einer 1:c Beziehung zwei gleiche Fremdschlüsselwerte angeben?**
> Das Einfügen gibt einen Fehler, falls ein UNIQUE-Constraint gesetzt ist.

1. **Was geschieht, wenn Sie z.B. bei einer 1:mc Beziehung NULL als FS-Wert angeben?**
> Es ist erlaubt, sofern kein NOT NULL-Constraint auf diesen Fremdschlüssel gesetzt ist.


# 4. Mengenlehre

Die Mengenlehre ist ein grundlegender Bereich der Mathematik, der sich mit der Untersuchung von Mengen befasst. Eine Menge ist eine Zusammenfassung von Objekten, die als Elemente der Menge bezeichnet werden. Diese Objekte können beliebig und verschiedener Art sein, wie Zahlen, Buchstaben oder andere mathematische Strukturen. Die Mengenlehre bildet die Basis für viele andere mathematische Disziplinen und wird verwendet, um Beziehungen und Strukturen innerhalb von Gruppen von Objekten zu analysieren und zu verstehen.

Element: Ein Objekt, das zu einer Menge gehört. Beispiel: In der Menge {1, 2, 3} sind 1, 2 und 3 Elemente.

Menge: Eine Sammlung von wohlunterschiedenen Objekten oder Elementen. Beispiel: Die Menge der natürlichen Zahlen {1, 2, 3, ...}.

Leere Menge: Eine Menge ohne Elemente. Sie wird durch {} oder ∅ dargestellt.

Teilmenge: Eine Menge A ist eine Teilmenge einer Menge B, wenn jedes Element von A auch ein Element von B ist. Dies wird als A ⊆ B notiert.

Vereinigung: Die Vereinigung zweier Mengen A und B ist die Menge aller Elemente, die in A oder B oder beiden enthalten sind. Notation: A ∪ B.

Durchschnitt: Der Durchschnitt zweier Mengen A und B ist die Menge aller Elemente, die sowohl in A als auch in B enthalten sind. Notation: A ∩ B.

Differenz: Die Differenz zweier Mengen A und B ist die Menge aller Elemente, die in A, aber nicht in B enthalten sind. Notation: A \ B.

# 5. Mengenlehre Aufgaben

#### 6.1 Aufgabe 1: Beurteilung der Aussagen zu Mengen
Gegeben sind die Mengen:
- A = {c, e, z, r, d, g, u, x}
- B = {c, e, g}
- C = {r, d, g, t}
- D = {e, z, u}
- E = {z, r, u}

**Beurteilung:**
- a) B ⊂ A - **Richtig**, alle Elemente von B sind in A enthalten.
- b) C ⊂ A - **Falsch**, da das Element t in C nicht in A enthalten ist.
- c) E ⊂ A - **Richtig**, alle Elemente von E sind in A enthalten.
- d) B ⊂ C - **Falsch**, c und e aus B sind nicht in C enthalten.
- e) E ⊂ C - **Falsch**, z und u aus E sind nicht in C enthalten.

#### 6.2 Aufgabe 2: Mengenoperationen

Gegeben sind die Mengen:
- ( A = {1, 2, 3, 4, 5} )
- ( B = {2, 5})
- ( C = {3, 5, 7, 9} )

**Bestimmung der Ergebnisse:**
- a) A ∩ B - **{2, 5}**
- b) A ∪ C - **{1, 2, 3, 4, 5, 7, 9}**
- c) B^c - 
- d) B \ C - **{2}**
- e) C \ B - **{3, 7, 9}**




